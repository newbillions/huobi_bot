#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import sched, time
import random
import Utils
import HuobiServices

MAX_RETRY = 3

def core_business_logic(s, SLEEP_TIME, MARKET):

    # Try to get market_accept_price
    retry_count = 0
    while (retry_count < MAX_RETRY):
        retry_count += 1
        try:
            order_book = HuobiServices.get_depth(MARKET, 'step0')['tick']
            break
        except:
            print("[ERROR]retry get_order_book "+str(retry_count))
            continue
    if retry_count >= MAX_RETRY:
        print("[ERROR] fail to get_order_book after {} trails. sleep for {} and wait for next time".format(MAX_RETRY,SLEEP_TIME))
        return

    bid_price = order_book['bids'][0][0]
    ask_price = order_book['asks'][0][0]
    market_accept_price = (ask_price + bid_price) / 2
    market_accept_price = round(market_accept_price,8)
    # market_accept_price = float(order_book[0][0])
    print("\t[INFO]our:%.8f\n" % (market_accept_price))
    #
    retry_count = 0
    while (retry_count < MAX_RETRY):
        retry_count += 1
        try:
            m_token_amount = random.randint(2000,5000)
            buy_order = HuobiServices.orders(m_token_amount, 'api', MARKET, 'buy-limit', market_accept_price)
            sell_order = HuobiServices.orders(m_token_amount, 'api', MARKET, 'sell-limit', market_accept_price)
            print("\tOrder placed. price {} amount {}\n\t{}\n\t{}".format(market_accept_price,m_token_amount,buy_order,sell_order))
        except Exception as e:
            print("[DEBUG] retry create_order {}count. {}".format(str(retry_count),e))
            if retry_count == MAX_RETRY: print("[ERROR]fail to create order after {} times. give up. {}".format(retry_count,e))
            continue

    s.enter(SLEEP_TIME, 1, core_business_logic, (s, SLEEP_TIME, MARKET))

Utils.ACCESS_KEY = "b3e42a7f-f54d1b07-8cf3d624-86341"
Utils.SECRET_KEY = "b635fee7-fda236c9-b26c0213-390a6"
MARKET = "smteth"
SLEEP_TIME = 1

s = sched.scheduler(time.time, time.sleep)
s.enter(1, 1, core_business_logic, (s,SLEEP_TIME,MARKET))
s.run()
